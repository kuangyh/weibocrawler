window.wb = window.wk || {};
(function() {

function getTextContent(html) {
  var el = document.createElement('div');
  el.style.display = 'none';
  el.innerHTML = html.replace(/<(?:.|\n)*?>/gm, '');
  return el.textContent;
}

wb.PostTableRow = React.createClass({
  render: function() {
    var data = this.props.data;
    var authorScreenName = (data['user'] || {})['screen_name'] || 'Unknown';
    var content = data['text'] || '';
    var link = "/admin/comments.html#" + parseInt(data['id']);
    return (
      <div className="post-row">
        <a href={link}>{getTextContent(content)} ({data['comments_count'] || 0})</a>
      </div>
    );
  }
});

wb.SearchBox = React.createClass({
  getInitialState: function() {
    return {'term': ''};
  },
  componentDidMount: function() {
    this.setState({'term': this.props.term});
  },
  componentWillReceiveProps: function(nextProps) {
    this.setState({'term': nextProps.term});
  },
  render: function() {
    return (
      <div className="search-box">
        <input type="text" placeholder="Search..." value={this.state.term}
          onChange={this.handleInputChange} onKeyUp={this.handleKeyUp}/>
      </div>);
  },
  handleInputChange: function(evt) {
    this.setState({'term': evt.target.value});
  },
  handleKeyUp: function(evt) {
    if (evt.keyCode == 13 && this.props.onSearch) {
      this.props.onSearch(this.state.term);
    }
  }
});

wb.PostsPage = React.createClass({
  getInitialState: function() {
    return {
      'searchTerm': '',
      'posts': [],
      'loading': false
    };
  },
  componentDidMount: function() {
    var searchTermFromHash = document.location.hash.substr(1);
    if (searchTermFromHash.length > 0) {
      this._search(searchTermFromHash);
    }
  },
  _search: function(term) {
    this.setState({
      'searchTerm': term,
      'loading': true
    });
    document.location.hash = '#' + term;

    var self = this;
    $.get('/post?screen_name=' + encodeURIComponent(term), function(result) {
      if (!self.isMounted()) {
        return;
      }
      result.sort(function(x, y) {return y.id - x.id});
      self.setState({
        'loading': false,
        'posts': result
      });
    });
  },
  render: function() {
    var self = this;
    var postWidgets = this.state.posts.map(function(post) {
      return <wb.PostTableRow key={post.id} data={post}/>
    });
    return (
      <div>
        <wb.SearchBox term={this.state.searchTerm} onSearch={this._search}/>
        {this.state.loading ? <div className="loading"/> : (
        <div className="posts">
          {postWidgets}
        </div>
        )}
      </div>
    );
  }
});

wb.PostDetailCard = React.createClass({
  render: function() {
    var renderData = this.props.data;
    return (
      <div className="post-detail-card">
        <div className="post-content">
          @{renderData['user']['screen_name']}:
          {getTextContent(renderData['text'])}
        </div>
        {(function () {
          if (renderData['retweeted_status']) {
            return (<div className="post-retweeted-status">
              @{renderData['retweeted_status']['user']['screen_name']}:
              {getTextContent(renderData['retweeted_status']['text'])}
            </div>);
          } else {
            return []
          }
        })()}
      </div>
    );
  }
});

wb.CommentLabelEditor = React.createClass({
  getInitialState: function() {
    var labels = [];
    for (var k in this.props.initialLabels) {
      labels.push(k);
    }
    labels.sort();
    return {
      'initLabelText': labels.join(' ')
    };
  },
  _onMaybeCommit: function(evt) {
    if (evt.keyCode == 13 && this.props.commitLabels) {
      var tmp = evt.target.value.trim();
      var labels = tmp.length > 0 ? tmp.split(/\s+/) : [];
      var labelMap = {};
      for (var i = 0; i < labels.length; i++) {
        labelMap[labels[i]] = {'source': 'admin'};
      }
      this.props.commitLabels(labelMap);
    }
  },
  render: function() {
    return <input type="text" className="comment-label-editor"
        defaultValue={this.state.initLabelText}
        ref={function(input) {if (input) { input.focus();} }}
        onKeyUp={this._onMaybeCommit}/>
  }
})

wb.CommentLabelList = React.createClass({
  render: function() {
    var labels = [];
    for (var k in this.props.data) {
      labels.push({'name': k, 'source': this.props.data[k]['source']});
    }
    labels.sort(function(l, r) {
      return l['name'] < r['name'] ? -1 : (l['name'] == r['name'] ? 0 : 1);});
    return (
      <div className="comment-label-list"> {
        labels.length > 0 ? (
          labels.map(function(label) {
            var labelClassName = (label['source'] == 'admin' ? 'comment-label-admin' : 'comment-label-model');
            return <span key={label.name} className={'comment-label ' + labelClassName}>{label.name}</span>
          })) : <span className="comment-label-placeholder">Click to add labels...</span>
      }
      </div>
    );
  }
});

wb.CommentTableRow = React.createClass({
  getInitialState: function() {
    return {
      'labelEditing': false,
    };
  },
  _onToggleLabelEditor: function() {
    if (!this.state.labelEditing) {
      this.setState({'labelEditing': true});
    }
  },
  _onSelectChanged: function(evt) {
    var selected = evt.target.checked;
    if (this.props.selectById) {
      this.props.selectById(this.props.data['id'], selected);
    }
  },
  _commitLabels: function(newLabelMap) {
    this.setState({
      'labelEditing': false
    });
    if (this.props.commitLabels) {
      this.props.commitLabels(this.props.data.id, newLabelMap);
    }
  },
  render: function() {
    var comment = this.props.data;
    return (
      <tr className="comment-table-row">
        <td className="selected-column">
          <input type="checkbox" checked={comment['selected'] || false} onChange={this._onSelectChanged} />
        </td>
        <td className = "content-column">
          <div className="content">
            @{comment['user']['screen_name']}: {getTextContent(comment['text'])}
          </div>
          <div className="labels" onClick={this._onToggleLabelEditor}>{
            this.state.labelEditing ?
              <wb.CommentLabelEditor
                  initialLabels={comment['labels'] || {}}
                  commitLabels={this._commitLabels}/> :
              <wb.CommentLabelList data={comment['labels'] || {}}/>
          }
          </div>
        </td>
      </tr>
    );
  }
});

wb.LabelEditor = React.createClass({
  getInitialState: function() {
    return {
      'filter': ''
    };
  },

  _onFilterChanged: function(evt) {
    var newValue = evt.target.value.trim();
    if (newValue == this.state.filter) {
      return;
    }
    this.setState({'filter': newValue})
    if (this.props.onFilter) {
      this.props.onFilter(newValue);
    }
  },

  _onSelectAll: function() {
    if (this.props.selectByFilter) {
      this.props.selectByFilter(this.state.filter, true);
    }
  },
  _onSelectNone: function() {
    if (this.props.selectByFilter) {
      this.props.selectByFilter(this.state.filter, false);
    }
  },
  _onActionChanged: function(evt) {
    if (evt.keyCode == 13 && this.props.commitActionToSelected) {
      this.props.commitActionToSelected(evt.target.value);
      evt.target.value = '';
    }
  },
  _onCommitChanges: function() {
    if (this.props.numChanged > 0 && this.props.uploadChanges) {
      this.props.uploadChanges();
    }
  },
  render: function() {
    return (
      <table className="label-editor"><tbody>
        <tr>
          <td className="editor-label">Filter:</td>
          <td>
            <input className="filter-input" type="text" onKeyUp={this._onFilterChanged} />
          </td>
        </tr>
        <tr>
          <td className="editor-label">Select:</td>
          <td>
            <div className="action-button" onClick={this._onSelectAll}>All</div>&nbsp;
            <div className="action-button" onClick={this._onSelectNone}>None</div>&nbsp;
            of {this.props.numFilteredComments} comments, {this.props.numSelected} selected,&nbsp;
            {this.props.numLabeled} / {this.props.numTotalComments} labeled.
          </td>
        </tr>
        <tr>
          <td className="editor-label">Do:</td>
          <td>
            <input className="action-input" type="text" onKeyUp={this._onActionChanged}/>
          </td>
        </tr>
        <tr>
          <td className="editor-label"></td>
          <td><div className="action-button" onClick={this._onCommitChanges}>
            Commit {this.props.numChanged} changes.
          </div></td>
        </tr>
      </tbody></table>
    );
  }
});

wb.CommentsPage = React.createClass({
  getInitialState: function() {
    return {
      'post': null,
      'comments': {},
      'changedIds': {},
      'filter': ''
    }
  },
  componentDidMount: function() {
    var postId = parseInt(document.location.hash.substr(1));
    if (!postId > 0) {
      return;
    }
    var self = this;
    $.get('/post?id=' + postId, function(result) {
      if (!self.isMounted() || !result.length > 0) {
        return;
      }
      self.setState({
        'post': result[0]
      });
    });
    $.get('/comment?post_id=' + postId, function(result) {
      if (!self.isMounted()) {
        return;
      }
      var comments = {};
      for (var i = 0; i < result.length; i++) {
        comments[parseInt(result[i]['id'])] = result[i];
      }
      self.setState({
        'comments': comments
      });
    });
  },
  _onFilterChanged: function(newFilter) {
    this.setState({'filter': newFilter});
  },
  _selectByFilter: function(filter, selected) {
    var filteredComments = this._filterAndSortComments(filter);
    for (var i = 0; i < filteredComments.length; i++) {
      var comment = filteredComments[i];
      this.state.comments[parseInt(comment['id'])]['selected'] = selected;
    }
    this.setState({'comments': this.state.comments});
  },
  _selectById: function(id, selected) {
    var comment = this.state.comments[parseInt(id)];
    if (comment) {
      comment.selected = selected;
    }
    this.setState({'comments': this.state.comments});
  },
  _commitLabelsById: function(commentId, newLabelMap) {
    var comment = this.state.comments[parseInt(commentId)];
    if (comment) {
      comment['labels'] = newLabelMap;
    }
    this.state.changedIds[parseInt(commentId)] = true;
    this.setState({
      'changedIds': this.state.changedIds,
      'comments': this.state.comments
    });
  },
  _commitActionToSelected: function(labelsAction) {
    var actions = labelsAction.trim().split(/\s+/);
    var needsReset = false;
    var addLabels = [];
    var removeLabels = [];
    for (var i = 0; i < actions.length; i++) {
      var action = actions[i];
      if (action.length == 0) {
        continue;
      }
      if (action == '!reset') {
        needsReset = true;
      } else if (action.startsWith('-')) {
        action = action.substr(1);
        if (action.length > 0) {
          removeLabels.push(action);
        }
      } else {
        addLabels.push(action);
      }
    }
    for (var id in this.state.comments) {
      var comment = this.state.comments[id];
      if (!comment.selected) {
        continue;
      }
      comment.selected = false;
      if (!comment.labels) {
        comment.labels = {};
      }
      var changed = false;
      if (needsReset) {
        comment.labels = {};
        changed = true;
      }
      for (var j = 0; j < addLabels.length; j++) {
        comment.labels[addLabels[j]] = {'source': 'admin'};
        changed = true;
      }
      for (var j = 0; j < removeLabels.length; j++) {
        delete comment.labels[removeLabels[j]];
        changed = true;
      }
      if (changed) {
        this.state.changedIds[id] = true;
      }
    }
    this.setState({
      'changedIds': this.state.changedIds,
      'comments': this.state.comments
    });
  },
  _filterAndSortComments: function(filter) {
    var result = [];
    var pattern = filter.length > 0 ? (new RegExp(this.state.filter, 'i')) : null;
    for (var k in this.state.comments) {
      var comment = this.state.comments[k];
      var matched = pattern ? pattern.exec(comment['text']) != null : true;
      if (matched) {
        result.push(comment);
      }
    }
    result.sort(function(l, r) {return r['id'] - l['ids']});
    return result;
  },
  _uploadChanges: function() {
    var changeList = [];
    for (var id in this.state.changedIds) {
      var comment = this.state.comments[id];
      changeList.push({
        'log_type': 'label',
        'comment_id': parseInt(id),
        'labels': comment['labels'] || {}
      });
    }
    var self = this;
    $.ajax({
      'type': 'POST',
      'url': '/log',
      'data': JSON.stringify(changeList),
      'contentType': 'application/json; charset=utf-8',
      'success': function() {
        self.setState({'changedIds': {}});
      }
    });
  },
  render: function() {
    var filteredComments = this._filterAndSortComments(this.state.filter);
    // Count selected comments
    var numSelected = 0;
    var numLabeled = 0;
    var numTotalComments = 0;
    for (var k in this.state.comments) {
      numTotalComments++;
      numSelected += this.state.comments[k].selected ? 1 : 0;
      if (this.state.comments[k]['labels']) {
        numLabeled++;
      }
    }
    var numChanged = 0;
    for (var k in this.state.changedIds) {
      numChanged++;
    }
    var self = this;
    return (
      <div>
        <div className="section">{
          this.state.post ?
          <wb.PostDetailCard data={this.state.post}/> :
          <div className="not-found">Post Not Found :-(</div>
        }
        </div>
        <div className="section">
          <wb.LabelEditor
            numFilteredComments={filteredComments.length}
            numTotalComments={numTotalComments}
            numSelected={numSelected}
            numChanged={numChanged}
            numLabeled={numLabeled}
            onFilter={this._onFilterChanged}
            selectByFilter={this._selectByFilter}
            commitActionToSelected={this._commitActionToSelected}
            uploadChanges={this._uploadChanges} />
        </div>
        <div className="section">
          <table className="comments-table"><tbody>{
            filteredComments.map(function(comment) {
              return <wb.CommentTableRow key={comment['id']} data={comment}
                selectById={self._selectById}
                commitLabels={self._commitLabelsById} />;
            })
          }</tbody></table>
        </div>
      </div>
    );
  }
});

// Render App
wb.initPostsPage = function() {
  ReactDOM.render(<wb.PostsPage/>, document.getElementById('app'));
};

wb.initCommentsPage = function() {
  ReactDOM.render(<wb.CommentsPage/>, document.getElementById('app'));
}

}).call(window);
