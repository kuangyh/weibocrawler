package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"
	"strconv"
	"time"
)

var (
	flagLogFile     = flag.String("logfile", "admin_trans.log", "Log file to add and restore")
	flagListenPort  = flag.Int("port", 8080, "port of server to listen")
	flagAdminAppDir = flag.String("admin_app", "admin", "Path to where admin Web app hosted")
)

type JsonObject map[string]interface{}

type AdminServer struct {
	logChn  chan []byte
	logFile *os.File

	posts    map[int]JsonObject
	comments map[int]JsonObject
}

func (server *AdminServer) Init(logFilename string) error {
	server.posts = make(map[int]JsonObject)
	server.comments = make(map[int]JsonObject)
	server.restore(logFilename)
	server.logChn = make(chan []byte)
	logFile, err := os.OpenFile(logFilename, (os.O_CREATE | os.O_WRONLY | os.O_APPEND), 0666)
	if err != nil {
		return err
	}
	server.logFile = logFile

	go func() {
		for {
			line := <-server.logChn
			// First log
			server.logFile.Write(line)
			server.logFile.Write([]byte{'\n'})
			// Then handle transaction
			server.handleTransactionSet(line)
		}
	}()
	return nil
}

func (server *AdminServer) restore(logFilename string) {
	f, err := os.Open(logFilename)
	if err != nil {
		log.Printf("New file %s, no need to restore", logFilename)
		return
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	log.Printf("Start restoring log from %s", logFilename)
	numRestoreRecords := 0
	startTime := time.Now()
	for scanner.Scan() {
		server.handleTransactionSet(scanner.Bytes())
		numRestoreRecords++
	}
	restoreDuration := time.Since(startTime)
	log.Printf("%d log records restored in %dms, serving %d posts, %d comments.",
		numRestoreRecords, restoreDuration/time.Millisecond, len(server.posts), len(server.comments))
}

func (server *AdminServer) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	switch request.URL.Path {
	case "/log":
		server.serveLog(writer, request)
	case "/post":
		server.servePost(writer, request)
	case "/comment":
		server.serveComment(writer, request)
	default:
		writer.WriteHeader(404)
		writer.Write([]byte(fmt.Sprintf("%s not found.", request.URL.Path)))
	}
}

// =================
// Data commit part
// =================

func (server *AdminServer) serveLog(writer http.ResponseWriter, request *http.Request) {
	if request.Method == "GET" {
		if err := request.ParseForm(); err != nil {
			writer.Write([]byte("INVALID FORM"))
			return
		}
		for _, item := range request.Form["data"] {
			server.logChn <- []byte(item)
		}
	} else {
		data, err := ioutil.ReadAll(request.Body)
		if err != nil {
			writer.Write([]byte("READ ERROR"))
			return
		}
		request.Body.Close()
		server.logChn <- data
	}
	writer.Write([]byte("ACK"))
}

func (server *AdminServer) handleTransactionSet(logLine []byte) {
	dataList := make([]JsonObject, 0)
	if err := json.Unmarshal(logLine, &dataList); err != nil {
		log.Printf("Corrupted transaction data %s", err)
	}
	for _, logItem := range dataList {
		logType := logItem["log_type"].(string)
		switch logType {
		case "post":
			server.commitPost(logItem)
		case "comment":
			server.commitComment(logItem)
		case "label":
			server.commitLabel(logItem)
		}
	}
}

func (server *AdminServer) commitPost(postJson JsonObject) {
	id, ok := postJson["id"].(float64)
	if !ok {
		log.Printf("commitPost failed, no id field in data")
		return
	}
	server.posts[int(id)] = postJson
}

func (server *AdminServer) commitComment(commentJson JsonObject) {
	id, ok := commentJson["id"].(float64)
	if !ok {
		log.Printf("commitComment failed, no id field in data")
		return
	}
	server.comments[int(id)] = commentJson
}

func (server *AdminServer) commitLabel(labelJson JsonObject) {
	id, ok := labelJson["comment_id"].(float64)
	if !ok {
		log.Printf("commitLabel failed, no comment_id field in data")
		return
	}
	labels, ok := labelJson["labels"]
	if !ok {
		log.Printf("commitLabel failed, no labels field in data")
		return
	}
	comment, ok := server.comments[int(id)]
	if !ok {
		log.Printf("commitLabel failed, no such comment_id=", id)
	}
	comment["labels"] = labels
}

// ================
// Data query part
// ================

func (server *AdminServer) servePost(writer http.ResponseWriter, request *http.Request) {
	if err := request.ParseForm(); err != nil || request.Method != "GET" {
		writer.WriteHeader(400)
		writer.Write([]byte("Invalid form"))
		return
	}

	result := make([]JsonObject, 0)
	if ids := request.Form["id"]; len(ids) > 0 {
		if id, err := strconv.Atoi(ids[0]); err == nil {
			post, ok := server.posts[id]
			if ok {
				result = append(result, post)
			}
		}
	} else if screenNames := request.Form["screen_name"]; len(screenNames) > 0 {
		result = filterRecordMap(makeScreenNameFilter(screenNames[0]), server.posts)
	}
	server.writeJSONResponse(writer, result)
}

func makeScreenNameFilter(name string) RecordFilter {
	return func(record JsonObject) bool {
		user, ok := record["user"].(map[string]interface{})
		if !ok {
			return false
		}
		return user["screen_name"].(string) == name
	}
}

func (server *AdminServer) serveComment(writer http.ResponseWriter, request *http.Request) {
	if err := request.ParseForm(); err != nil || request.Method != "GET" {
		writer.WriteHeader(400)
		writer.Write([]byte("Invalid form"))
		return
	}
	result := make([]JsonObject, 0)
	if postIds := request.Form["post_id"]; len(postIds) > 0 {
		if postId, err := strconv.Atoi(postIds[0]); err == nil {
			result = filterRecordMap(makePostIdFilter(postId), server.comments)
		}
	}
	server.writeJSONResponse(writer, result)
}

func makePostIdFilter(postId int) RecordFilter {
	return func(record JsonObject) bool {
		recordPostId, ok := record["post_id"].(float64)
		if !ok {
			return false
		}
		return int(recordPostId) == postId
	}
}

// =================================================
// Common routine of filtering and HTTP JSON output
// =================================================
type RecordFilter func(JsonObject) bool

type sortByIdDesc []JsonObject

func (ls sortByIdDesc) Len() int {
	return len(ls)
}

func (ls sortByIdDesc) Less(i int, j int) bool {
	return ls[i]["id"].(float64) > ls[j]["id"].(float64)
}

func (ls sortByIdDesc) Swap(i int, j int) {
	tmp := ls[i]
	ls[i] = ls[j]
	ls[j] = tmp
}

func filterRecordMap(fn RecordFilter, records map[int]JsonObject) []JsonObject {
	output := make(sortByIdDesc, 0)
	for _, item := range records {
		if fn(item) {
			output = append(output, item)
		}
	}
	sort.Sort(output)
	return []JsonObject(output)
}

func (server *AdminServer) writeJSONResponse(writer http.ResponseWriter, data interface{}) {
	jsonBytes, err := json.Marshal(data)
	if err != nil {
		writer.WriteHeader(502)
		writer.Write([]byte("Internal error when marshal json."))
		return
	}
	writer.Header()["Content-Type"] = []string{"text/json; charset=utf-8"}
	writer.Write(jsonBytes)
}

func main() {
	flag.Parse()
	// ICS WebApp
	http.Handle(
		"/admin/", http.StripPrefix("/admin/", http.FileServer(http.Dir(*flagAdminAppDir))))
	// The server
	server := AdminServer{}
	if err := server.Init(*flagLogFile); err != nil {
		panic(err)
	}
	http.Handle("/", &server)

	http.ListenAndServe(fmt.Sprintf(":%d", *flagListenPort), nil)
}
