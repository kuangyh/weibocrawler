import HTMLParser
import json
import re
import unicodedata

import jieba.jieba

def ExtractTerms(text):
  output = []
  for word in jieba.jieba.cut(text):
    word = word.strip()
    if len(word) == 0:
      continue
    if unicodedata.category(word[0]).startswith('P'):
      continue
    if ord(word[0]) < 255:
      # Assume latin
      word = word.lower()
    output.append(word)
  return output

def GetTextContent(html_str):
  'Filter all HTML tags then unescape to get raw text of content'
  parser = HTMLParser.HTMLParser()
  text = parser.unescape(re.sub(r'<.+?>', ' ', html_str))
  # clean up emoji
  text = re.sub(r'\[\S+?\]', ' ', text)
  # TODO: clean up url?
  return text.strip()

def LogFile(filename):
  for line in open(filename):
    data = json.loads(line)
    if type(data) is list:
      for item in data:
        yield item
    else:
      yield data
