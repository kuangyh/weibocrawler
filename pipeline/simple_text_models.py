#!/usr/sbin/env python
# -*- coding: utf-8 -*-
import json
import utils
import math
import logging

class DataSet(object):
  def __init__(self):
    self.posts = {}
    self.posts

def LoadComments(filename):
  comments = []
  for item in utils.LogFile(filename):
    if item['log_type'] == 'comment':
      comments.append({
        'id': item['id'],
        'post_id': item['post_id'],
        'text': utils.GetTextContent(item['text']),
        'author': item['user']['screen_name']
      })
  return comments

def LoadPosts(filename):
  posts = []
  for item in utils.LogFile(filename):
    if item['log_type'] == 'post':
      posts.append({
        'id': item['id'],
        'text': utils.GetTextContent(item['text']),
        'author': item['user']['screen_name']
      })
  return posts


class FreqDist(object):
  def __init__(self):
    self.data = {}
    self.total = 0

  def Add(self, item):
    self.data[item] = self.data.get(item, 0) + 1
    self.total += 1

  def GetFreq(self, item):
    return self.data.get(item, 0)

  def GetProp(self, item):
    if self.total == 0:
      return 0.0
    else:
      return float(self.GetFreq(item)) / float(self.total)

  def Terms(self):
    return self.data.keys()


class PostTfIdfModel(object):
  def __init__(self):
    self.overall_freq = FreqDist()
    self.post_freq = {}
    self.comments = []

  def LoadComments(self, comments):
    self.comments = comments
    comments_by_author = {}
    for comment in comments:
      comments_by_author.setdefault(
          (comment['post_id'], comment['author']), []).append(comment)
    for key, comments in comments_by_author.iteritems():
      terms = set(utils.ExtractTerms(
          ' '.join([x['text'].split('//', 1)[0] for x in comments])))
      for term in terms:
        self.post_freq.setdefault(key[0], FreqDist()).Add(term)
    for post_id, freq in self.post_freq.iteritems():
      for term in freq.Terms():
        self.overall_freq.Add(term)

  def GetResult(self):
    result = {}
    for post_id, freq in self.post_freq.iteritems():
      terms = []
      for term in freq.Terms():
        tf = math.log(freq.GetFreq(term))
        idf = math.log(len(self.post_freq) / self.overall_freq.GetFreq(term))
        terms.append({'term': term, 'freq': freq.GetFreq(term), 'tf-idf': tf * idf})
      terms.sort(key=lambda x: -x['tf-idf'])
      result[post_id] = terms
    return result

def GenerateReport(log_filename, report_filename):
  logging.info('Loading comment data...')
  comments = LoadComments(log_filename)
  logging.info('Calculating model...')
  model = PostTfIdfModel()
  model.LoadComments(comments)
  result = model.GetResult()
  logging.info('Generating report...')
  posts = LoadPosts(log_filename)
  fp = open(report_filename, 'w')
  for post in posts:
    fp.write((u'%s: %s\n' % (post['author'], post['text'])).encode('utf-8'))
    for term in result.get(post['id'], [])[:20]:
      fp.write((u'  %(term)s (%(freq)d, %(tf-idf)0.4f)\n' % term).encode('utf-8'))
    fp.write('----\n\n')

if __name__ == '__main__':
  GenerateReport('dev.log', 'dev-report.txt')






