#!/usr/bin/env python

import re
import HTMLParser
import json
import sys

def GetTextContent(html_str):
  'Filter all HTML tags then unescape to get raw text of content'
  parser = HTMLParser.HTMLParser()
  return parser.unescape(re.sub(r'<.+?>', '', html_str))

def ExtractUser(user_json):
  return {
    'id': user_json['id'],
    'screen_name': user_json['screen_name']
  }

def ExtratPost(post_json):
  text = GetTextContent(post_json.get('text', ''))
  if 'retweeted_status' in post_json:
    text += ' ' + GetTextContent(post_json['retweeted_status'].get(text, ''))

  return {
    'id': post_json['id'],
    'user': ExtractUser(post_json['user']),
    'is_repost': 'retweeted_status' in post_json,
    'text': text,
    'reposts_count': post_json.get('reposts_count', 0),
    'comments_count': post_json.get('comments_count', 0),
    'like_count': post_json.get('like_count', 0),
    'comments': [],
  }

def ExtractComment(comment_json):
  return {
    'id': comment_json['id'],
    'post_id': comment_json['post_id'],
    'text': GetTextContent(comment_json['text']).split('//', 1)[0]
  }

if __name__ == '__main__':
  posts = {}
  for line in sys.stdin:
    try:
      data_list = json.loads(line)
    except Exception, e:
      continue
    for item in data_list:
      if item['log_type'] == 'post':
        post = ExtratPost(item)
        if post['id'] not in posts:
          posts[post['id']] = post
      elif item['log_type'] == 'comment':
        comment = ExtractComment(item)
        if comment['post_id'] in posts:
          posts[comment['post_id']]['comments'].append(comment)
        else:
          print 'no related post for %d' % (comment['post_id'])
  posts = posts.values()
  posts.sort(key = lambda x: x['id'])
  json.dump(posts, open('output.json', 'w'))


