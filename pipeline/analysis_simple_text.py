#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

import utils
import math

class TermDist(object):
  def __init__(self):
    self.data = {}
    self.max_freq = 0
    self.total = 0

  def Add(self, term):
    freq = self.data.get(term, 0) + 1
    self.data[term] = freq
    if freq > self.max_freq:
      self.max_freq = freq
    self.total += 1

  def GetTerms(self):
    return self.data.keys()

  def GetFreq(self, term):
    return self.data.get(term, 0)

  def GetTf(self, term):
    freq = self.data.get(term, 0)
    if freq == 0:
      return 0.0
    return 0.5 + 0.5 * freq / float(self.max_freq)

  def GetIdf(self, term, num_docs):
    freq = self.data.get(term, 0)
    if freq == 0:
      return -1
    return math.log(float(num_docs) / float(freq))

class TfIdfModel(object):
  def __init__(self, dataset):
    self.dataset = dataset
    self.overall_dist = TermDist()
    self.post_dist_map = {}

  def Perform(self):
    for post in self.dataset:
      post_id = post['id']
      post_dist = TermDist()
      self.post_dist_map[post_id] = post_dist
      for comment in post['comments']:
        for term in utils.ExtractTerms(comment['text']):
          post_dist.Add(term)
      for term in post_dist.GetTerms():
        self.overall_dist.Add(term)
    result_list = []
    for post in self.dataset:
      post_result = {
        'id': post['id'],
        'text': post['text'],
      }
      keywords = []
      post_dist = self.post_dist_map[post['id']]
      for term in post_dist.GetTerms():
        tf_idf = post_dist.GetTf(term) * self.overall_dist.GetIdf(term, len(self.dataset))
        keywords.append({
          'term': term,
          'tf-idf': tf_idf,
          'term-freq': post_dist.GetFreq(term),
          'doc-freq': self.overall_dist.GetFreq(term)
        })
      keywords.sort(key = lambda x: -x['tf-idf'])
      post_result['keywords'] = keywords
      result_list.append(post_result)
    return result_list


class KeywordModel(object):
  def __init__(self, dataset):
    self.dataset = dataset
    self.keywords = [u'衣', u'鞋', u'美', u'爱你']

  def Perform(self):
    result_list = []
    for post in self.dataset:
      post_result = {
        'id': post['id'],
        'text': post['text'],
      }
      result_list.append(post_result)
      cluster = {}
      for comment in post['comments']:
        for keyword in self.keywords:
          if keyword in comment['text']:
            cluster.setdefault(keyword, []).append(comment['text'])
      post_result['comment_cluster'] = cluster
      post_result['comments_count'] = len(post['comments'])
    return result_list

if __name__ == '__main__':
  dataset = json.load(open('data/baibaihe.text.json'))
  model = KeywordModel(dataset)
  for post in model.Perform():
    print post['text'].encode('utf-8')
    print '  comments_count: %d' % (post['comments_count'],)
    for cluster_name, comments in post['comment_cluster'].iteritems():
      print '  Cluster: %s %d' % (cluster_name.encode('utf-8'), len(comments))
      print '    ' + u'\n    '.join(comments).encode('utf-8')
      print ''
    print '--\n\n'

  # model = TfIdfModel(dataset)
  # for post in model.Perform():
  #   print post['text'].encode('utf-8')
  #   for keyword in post['keywords'][:20]:
  #     print (u'  %(term)s\t%(tf-idf)0.4f\t%(term-freq)d\t%(doc-freq)d' % keyword).encode('utf-8')
  #   print '--\n'
