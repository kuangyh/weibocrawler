window.wbcrawl = {};
(function() {

wbcrawl.TaskQueue = function(crawlIntervalMs) {
  this._crawlIntervalMs = crawlIntervalMs || 3000;
  this._lastTaskTime = 0;
  this._numRunningTasks = 0;
  this._numFailures = 0;
  this._pendingQueue = [];
  this.ui = null;
};

wbcrawl.TaskQueue.prototype.add = function(task) {
  this._pendingQueue.push(task);
  if (this._numRunningTasks <= 0) {
    this._performQueue();
  }
};

wbcrawl.TaskQueue.prototype._performQueue = function() {
  if (this.ui) {
    this.ui.modifyData({
      'pendings': this._numRunningTasks + this._pendingQueue.length,
      'failures': this._numFailures,
      'crawling': this._numRunningTasks + this._pendingQueue.length > 0
    });
  }
  if (this._pendingQueue.length == 0) {
    return;
  }
  var delay = Math.max(
      10, this._lastTaskTime + this._crawlIntervalMs * (1 + Math.random()) - Date.now());
  var task = this._pendingQueue.shift();
  var self = this;
  this._numRunningTasks++;
  setTimeout(function() {
    self._performTask(task);
  }, delay);
};

wbcrawl.TaskQueue.prototype._performTask = function(task) {
  var self = this;
  var url = task['url'];
  this._lastTaskTime = Date.now();
  if (!url) {
    return;
  }
  var xhrHandleError = function(msg) {
    console.log('TASK ' + url + ' failed at state ' + xhr.readyState + ' ' + msg);
    self._numFailures++;
    self._numRunningTasks--;
    self._performQueue();
  }

  var xhr = new XMLHttpRequest();
  xhr.onerror = xhr.onabort = xhr.ontimeout = xhrHandleError;
  xhr.onload = function() {
    if (xhr.status != 200) {
      return xhrHandleError('HTTP status ' + xhr.status);
    }
    try {
      task['content'] = xhr.responseText;
      var result = task['handler'](task);
      if (result != null) {
        self._handleTaskResult(task, result);
      }
    } catch (e) {
      return xhrHandleError('handler error ' + e);
    }
    self._numRunningTasks--;
    self._performQueue();
  };
  console.log('TASK ' + url + ' started.');
  xhr.open('GET', url, true);
  xhr.send();
};

wbcrawl.TaskQueue.prototype._handleTaskResult = function(task, result) {
  var onTaskDone = this.ontaskdone;
  var nextTasks = result['next'];
  if (nextTasks != null) {
    for (var i = 0; i < nextTasks.length; i++) {
      this.add(nextTasks[i]);
    }
  }
  var upload = result['upload'];
  if (upload == null) {
    return;
  }
  var xhr = new XMLHttpRequest();
  xhr.onload = function() {
    console.log('UPLOAD ' + task['url'] + ' server returns ' + xhr.responseText);
    if (onTaskDone) {
      onTaskDone(url);
    }
  }
  xhr.onerror = xhr.onabort = xhr.ontimeout = function() {
    console.log('UPLOAD ' + task['url'] + ' request fail at readyState ' + xhr.readyState);
  }
  xhr.open('POST', 'http://localhost:8080/log');
  xhr.setRequestHeader('Content-type', 'text/json; charset=utf-8');
  xhr.send(JSON.stringify(upload));
}

wbcrawl.taskQueue = new wbcrawl.TaskQueue();

wbcrawl.spawnTask = function(orig, pairs) {
  var output = {};
  for (var k in orig) {
    if (orig.hasOwnProperty(k)) {
      output[k] = orig[k];
    }
  }
  if (pairs) {
    for (var k in pairs) {
      if (pairs.hasOwnProperty(k)) {
        output[k] = pairs[k];
      }
    }
  }
  return output;
}

wbcrawl.getFeedURL = function(containerId, pageId) {
  return 'http://m.weibo.cn/page/json?containerid=' + containerId + '&page=' + pageId;
}

wbcrawl.getCommentURL = function(postId, pageId) {
  return 'http://m.weibo.cn/single/rcList?format=cards&id=' +
    postId + '&type=comment&hot=0&tab=2&page=' + pageId;
}

wbcrawl.parseFeed = function(task) {
  var data = JSON.parse(task.content);
  var parseResult = [];

  var itemList = ((data['cards'] || [])[0] || {})['card_group'] || [];
  if (itemList.length == 0 && task.allowRetry > 0) {
    console.log('Retry task ' + task.url);
    return {
      'next': [wbcrawl.spawnTask(task, {'allowRetry': task.allowRetry - 1})]
    }
  }
  var uploadData = [];
  var postIds = [];
  for (var i = 0; i < itemList.length; i++) {
    var mblogData = itemList[i]['mblog'];
    if (!mblogData) {
      continue;
    }
    mblogData['log_type'] = 'post';
    uploadData.push(mblogData);
    var postId = mblogData['id'];
    if (postId) {
      postIds.push(postId);
    }
  }
  var nextTasks = [];
  if (task.allowNextPage > 0) {
    nextTasks.push(wbcrawl.spawnTask(task, {
      'url': wbcrawl.getFeedURL(task.containerId, task.page + 1),
      'page': task.page + 1,
      'allowNextPage': task.allowNextPage - 1,
      'allowRetry': task.maxAttempts - 1,
    }));
  }
  for (var i = 0; i < postIds.length; i++) {
    nextTasks.push({
      'url': wbcrawl.getCommentURL(postIds[i], 1),
      'handler': wbcrawl.parseCommentList,
      'ui': task.ui,
      'postId': postIds[i],
      'page': 1,
      'allowNextPage': task.maxCommentPages - 1,
    });
  }
  if (task.ui) {
    if (itemList.length > 0) {
      task.ui.modifyData({'posts': task.ui.data.posts + itemList.length});
    } else {
      task.ui.modifyData({'failures': task.ui.data.failures + 1});
    }
  }
  console.log(nextTasks);
  return {
    'next': nextTasks,
    'upload': uploadData
  };
}

wbcrawl.parseCommentList = function(task) {
  var data = JSON.parse(task.content);
  var uploadData = [];
  for (var i = 0; i < data.length; i++) {
    if (data[i]['mod_type'] == 'mod/pagelist') {
      var itemList = data[i]['card_group'] || [];
      for (var j = 0; j < itemList.length; j++) {
        var item = itemList[j];
        item['log_type'] = 'comment';
        item['post_id'] = task.postId;
        uploadData.push(item);
      }
    } else if (data[i]['mod_type'] == 'mod/tab') {
      // Handle recursive structure of cards
      var tabs = data[i]['tabs'] || [];
      for (var j = 0; j < tabs.length; j++) {
        data = data.concat(tabs[j]['cards'] || []);
      }
    }
  }
  var nextTasks = [];
  if (uploadData.length > 0 && task.allowNextPage > 0) {
    nextTasks.push(wbcrawl.spawnTask(task, {
      'url': wbcrawl.getCommentURL(task.postId, task.page + 1),
      'page': task.page + 1,
      'allowNextPage': task.allowNextPage - 1
    }));
  }
  if (task.ui) {
    task.ui.modifyData({'comments': task.ui.data.comments + uploadData.length});
  }
  return {
    'next': nextTasks,
    'upload': uploadData.length > 0 ? uploadData : null
  };
}

wbcrawl.UIController = function(dom) {
  this.titleDom = dom.querySelector('.crawl-title');
  this.numPostsDom = dom.querySelector('.num-posts');
  this.numCommentsDom = dom.querySelector('.num-comments');
  this.numPendingsDom = dom.querySelector('.num-pendings');
  this.numFailuresDom = dom.querySelector('.num-failures');
  this.buttonDom = dom.querySelector('.crawl-button');
  this.setData({
    'title': '',
    'url': '',
    'crawling': false,
    'posts': 0,
    'comments': 0,
    'pendings': 0,
    'failures': 0
  });
  wbcrawl.taskQueue.ui = this;
  var self = this;
  this.buttonDom.onclick = function() {
    self.crawl();
  };

  var taskInfo = document.location.hash.substr(1).split('|');
  self.modifyData({
    'title': taskInfo[1],
    'url': taskInfo[0]
  });
};

wbcrawl.UIController.prototype.setData = function(data) {
  this.data = data;
  this.titleDom.textContent = data.title;
  this.numPostsDom.textContent = '' + data.posts;
  this.numCommentsDom.textContent = '' + data.comments;
  this.numPendingsDom.textContent = '' + data.pendings;
  this.numFailuresDom.textContent = '' + data.failures;
  this.buttonDom.value = data.crawling ? 'Crawling' : 'Start Crawl';
  this.buttonDom.disabled = (
      data.crawling ||
      !(data.url.startsWith('http://m.weibo.cn/page/tpl?containerid')));
};

wbcrawl.UIController.prototype.modifyData = function(pairs) {
  for (var k in pairs) {
    if (pairs.hasOwnProperty(k)) {
      this.data[k] = pairs[k];
    }
  }
  this.setData(this.data);
}

wbcrawl.UIController.prototype.crawl = function() {
  this.modifyData({'crawling': true});
  var query = /containerid=([^\&]+)/.exec(this.data.url);
  if (query.length < 2) {
    console.log('Invalid entry point ' + this.data.url);
    return;
  }

  wbcrawl.taskQueue.add({
    'url': wbcrawl.getFeedURL(query[1], 1),
    'handler': wbcrawl.parseFeed,
    'page': 1,
    'containerId': query[1],
    'allowRetry': 1,
    'allowNextPage': 4,
    'maxCommentPages': 100,
    'maxAttempts': 2,
    'ui': this
  });
};

wbcrawl.ui = null;

// App entry point.
wbcrawl.app = function() {
  wbcrawl.ui = new wbcrawl.UIController(document.querySelector('.popup'));
};

}).call(window);
// Starting up application
wbcrawl.app();
