document.querySelector('#start').onclick = function() {
  chrome.tabs.query(
      {'active': true, 'windowId': chrome.windows.WINDOW_ID_CURRENT},
      function(result) {
    if (result.length == 0) {
      return;
    }
    var tab = result[0];
    window.open('main.html#' + tab.url + '|' + tab.title);
  });
}
